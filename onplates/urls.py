from django.conf.urls import patterns, include, url

from django.contrib import admin
from onplates import settings

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    url(r'^admin/', include(admin.site.urls)),

    (r'^media/(.*)$', 'django.views.static.serve',
     {'document_root': settings.MEDIA_ROOT}),
    url(r'^placa-interna/(?P<place_id>[\w_-]+)/$', 'core.views.placeIntern', name='interna'),
    url(r'^$', 'core.views.homepage', name='homepage'),
)
