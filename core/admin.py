from django.contrib import admin
from core.models import UserProfile, Album, Photo, Course, Institution, Forming, Place, Video, TextBlock, PhotoBlock

__author__ = 'Wouker'

admin.site.register(UserProfile)
admin.site.register(Album)
admin.site.register(Photo)
admin.site.register(Course)
admin.site.register(Institution)
admin.site.register(Forming)
admin.site.register(Place)
admin.site.register(Video)
admin.site.register(TextBlock)
admin.site.register(PhotoBlock)