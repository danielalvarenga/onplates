# Create your views here.
from django.shortcuts import render_to_response
from django.template import RequestContext
from core.models import Place


def homepage(request):
    places = Place.objects.all()
    return render_to_response('placas.html', locals(), context_instance=RequestContext(request))


def placeIntern(request, place_id):
    place = Place.objects.get(pk=place_id)
    return render_to_response('home.html', locals(), context_instance=RequestContext(request))