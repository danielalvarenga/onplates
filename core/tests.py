# -*- coding: utf-8 -*-
"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.utils import unittest
from django.core.files import File
from core.models import *
from onplates.settings import MEDIA_ROOT


class SetUp(unittest.TestCase):
    img = File(open(MEDIA_ROOT + '/photos/koala.jpg'))

    def setUp(self):
        # Instanciando objeto UserProfile
        self.user_profile = UserProfile.objects.create(name='Daniel Alvarenga',
                                                       email='danielalvarenga@gmail.com')
        # Instanciando objeto Institution
        self.institution = Institution.objects.create(name='IFPI',
                                                      country='Brasil',
                                                      uf='Piauí',
                                                      city='Teresina')
        # Instanciando objeto Course
        self.course = Course.objects.create(name='ADS')
        # Instanciando objeto Forming
        self.forming = Forming.objects.create(name='Nataliel Vasconcelos',
                                              email='nataliel@gmail.com',
                                              img_perfil=self.img)
        # Instanciando objeto Plate
        self.place = Place.objects.create(team_name='Turma Legal',
                                          course=self.course,
                                          institution=self.institution,
                                          user_profile=self.user_profile)
        # Instanciando objeto Album
        self.album = Album.objects.create(name='teste album',
                                          description='descrição do album',
                                          place=self.place)
        # Instanciando objeto Photo
        self.photo = Photo.objects.create(description='descrição da foto',
                                          img=self.img,
                                          album=self.album)
        # Instanciando objeto Video
        self.video = Video.objects.create(name='Vídeo da Turma',
                                 description='descrição do vídeo',
                                 url='http://youtube.com',
                                 place=self.place)
        #Instanciando objeto TextBlock
        self.text_block = TextBlock.objects.create(name='Título',
                                          text_area='Texto maior formatável',
                                          author='Fulanim',
                                          place=self.place)
        # Instanciando objeto PhotoBlock
        self.photo_block = PhotoBlock.objects.create(description='descrição da foto',
                                                  img=self.img,
                                                  place=self.place)


    def tearDown(self):
        self.photo.img.delete(save=True)
        self.forming.img_perfil.delete(save=True)
        self.photo_block.img.delete(save=True)


class TestUserProfile(SetUp):
    def test_name(self):
        self.assertEqual(self.user_profile.name, 'Daniel Alvarenga')

    def test_email(self):
        self.assertEqual(self.user_profile.email, 'danielalvarenga@gmail.com')

    def test_date(self):
        self.assertEqual(self.user_profile.date.date(), datetime.now().date())


class TestCourse(SetUp):
    def test_name(self):
        self.assertEqual(self.course.name, 'ADS')


class TestInstitution(SetUp):
    def test_name(self):
        self.assertEqual(self.institution.name, 'IFPI')

    def test_city(self):
        self.assertEqual(self.institution.city, 'Teresina')

    def test_uf(self):
        self.assertEqual(self.institution.uf, 'Piauí')

    def test_country(self):
        self.assertEqual(self.institution.country, 'Brasil')


class TestForming(SetUp):
    def test_name(self):
        self.assertEqual(self.forming.name, 'Nataliel Vasconcelos')

    def test_email(self):
        self.assertEqual(self.forming.email, 'nataliel@gmail.com')

    def test_img_perfil(self):
        self.failUnless(self.forming.img_perfil.path, 'Image not found')


class TestPlace(SetUp):
    def test_team_name(self):
        self.assertEquals(self.place.team_name, 'Turma Legal')

    def test_date(self):
        self.assertEqual(self.place.date.date(), datetime.now().date())

    def test_course(self):
        self.assertEqual(self.place.course, self.course)

    def test_intitution(self):
        self.assertEqual(self.place.institution, self.institution)

    def test_user_profile(self):
        self.assertEqual(self.place.user_profile, self.user_profile)

    def test_formings(self):
        self.place.formings.add(self.forming)
        self.assertEqual(len(self.place.formings.all()), 1)


class testAlbum(SetUp):
    def test_name(self):
        self.assertEquals(self.album.name, 'teste album')

    def test_description(self):
        self.assertEqual(self.album.description, 'descrição do album')

    def test_date(self):
        self.assertEqual(self.album.date.date(), datetime.now().date())

    def test_place(self):
        self.assertEqual(self.album.place, self.place)


class TestPhoto(SetUp):
    def test_description(self):
        self.assertEqual(self.photo.description, 'descrição da foto')

    def test_date(self):
        self.assertEqual(self.photo.date.date(), datetime.now().date())

    def test_img(self):
        self.failUnless(open(self.photo.img.path), 'Image not found')

    def test_album(self):
        self.assertEqual(self.photo.album, self.album)


class TestVideo(SetUp):
    def test_name(self):
        self.assertEquals(self.video.name, 'Vídeo da Turma')

    def test_description(self):
        self.assertEqual(self.video.description, 'descrição do vídeo')

    def test_date(self):
        self.assertEqual(self.video.date.date(), datetime.now().date())

    def test_place(self):
        self.assertEqual(self.video.place, self.place)

    def test_url(self):
        self.assertEquals(self.video.url, 'http://youtube.com')


class TestTextBlock(SetUp):
    def test_name(self):
        self.assertEquals(self.text_block.name, 'Título')

    def test_text_area(self):
        self.assertEqual(self.text_block.text_area, 'Texto maior formatável')

    def test_place(self):
        self.assertEqual(self.text_block.place, self.place)

    def test_author(self):
        self.assertEquals(self.text_block.author, 'Fulanim')


class TestPhotoBlock(SetUp):
    def test_description(self):
        self.assertEqual(self.photo_block.description, 'descrição da foto')

    def test_img(self):
        self.failUnless(open(self.photo_block.img.path), 'Image not found')

    def test_place(self):
        self.assertEqual(self.photo_block.place, self.place)