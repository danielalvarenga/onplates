from datetime import datetime
from django.db import models
from django.template.defaultfilters import default


class UserProfile(models.Model):
    name = models.CharField(max_length=50)
    email = models.EmailField()
    date = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.name


class Course(models.Model):
    name = models.CharField(max_length=50)

    def __unicode__(self):
        return self.name


class Institution(models.Model):
    name = models.CharField(max_length=100)
    city = models.CharField(max_length=50)
    uf = models.CharField(max_length=50)
    country = models.CharField(max_length=50)

    def __unicode__(self):
        return self.name


class Forming(models.Model):
    name = models.CharField(max_length=50)
    email = models.EmailField(null=True, blank=True)
    img_perfil = models.ImageField(upload_to='img_perfil')

    def __unicode__(self):
        return self.name


class Place(models.Model):
    team_name = models.CharField(max_length=50)
    date = models.DateTimeField(auto_now_add=True)
    course = models.ForeignKey(Course)
    institution = models.ForeignKey(Institution)
    user_profile = models.ForeignKey(UserProfile)
    formings = models.ManyToManyField(Forming, null=True, blank=True)

    def __unicode__(self):
        return self.team_name


class Album(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    place = models.ForeignKey(Place)

    def __unicode__(self):
        return self.name


class Photo(models.Model):
    description = models.TextField(null=True, blank=True)
    img = models.ImageField(upload_to="photos")
    date = models.DateTimeField(auto_now_add=True)
    album = models.ForeignKey(Album)


class Video(models.Model):
    place = models.ForeignKey(Place)
    name = models.CharField(max_length=50)
    description = models.TextField()
    url = models.TextField(blank=False, null=False)
    date = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.name


class TextBlock(models.Model):
    place = models.ForeignKey(Place)
    name = models.CharField(max_length=100)
    text_area = models.TextField()
    author = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name


class PhotoBlock(models.Model):
    place = models.ForeignKey(Place)
    name = models.CharField(max_length=50)
    description = models.TextField()
    img = models.ImageField(upload_to='img_blocks')

    def __unicode__(self):
        return self.name